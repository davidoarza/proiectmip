package com.university.project.model;

import java.util.List;

public class UserDTO {
    private long id_user;
    private String name;
    private List<ListDTO> list;

    public UserDTO() {
    }

    public UserDTO(long id_user, String name, List<ListDTO> list) {
        this.id_user = id_user;
        this.name = name;
        this.list = list;
    }

    public long getIdUser() {
        return id_user;
    }

    public void setIdUser(long id_user) {
        this.id_user = id_user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId_user() {
        return id_user;
    }

    public void setId_user(long id_user) {
        this.id_user = id_user;
    }

    public List<ListDTO> getList() {
        return list;
    }

    public void setList(List<ListDTO> list) {
        this.list = list;
    }
}
