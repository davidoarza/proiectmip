package com.university.project.model;

public class ActivityDTO {
    private long id;
    private String activity;
    private ListDTO list;

    public ActivityDTO() {
    }

    public ActivityDTO(long id_activity, String activity, ListDTO list) {
        this.id = id_activity;
        this.activity = activity;
        this.list = list;
    }

    public long getId() {
        return id;
    }

    public void setId(long id_activity) {
        this.id = id_activity;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public ListDTO getList() {
        return list;
    }

    public void setList(ListDTO list) {
        this.list = list;
    }
}