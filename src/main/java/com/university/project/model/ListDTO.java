package com.university.project.model;

import java.util.List;

public class ListDTO {
    private long id;
    private UserDTO user;
    private List<ActivityDTO> activity;

    public ListDTO() {
    }

    public ListDTO(long id, UserDTO user, List<ActivityDTO> activity) {
        this.id = id;
        this.user = user;
        this.activity = activity;
    }

    public long getIdList() {
        return id;
    }

    public void setIdList(long id_list) {
        this.id = id_list;
    }

    public UserDTO getIdUser() {
        return user;
    }

    public void setIdUser(UserDTO id_user) {
        this.user = user;
    }

    public List<ActivityDTO> getActivity() {
        return activity;
    }

    public void setActivity(List<ActivityDTO> activity) {
        this.activity = activity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }
}