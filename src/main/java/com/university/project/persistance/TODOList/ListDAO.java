package com.university.project.persistance.TODOList;

import com.university.project.model.ListDTO;
import com.university.project.model.UserDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "List")
public class ListDAO implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private UserDAO us;

    @OneToMany(mappedBy = "list")
    private List<ActivityDAO> activities = new ArrayList<>();

    @ManyToOne
    UserDAO user;

    public ListDAO() {
    }

    public ListDAO(Long id, UserDAO user, List<ActivityDAO> activities) {
        this.id = id;
        this.user = user;
        this.activities = activities;
    }

    public long getIdList() {
        return id;
    }

    public void setIdList(long id_list) {
        this.id = id_list;
    }


    public UserDAO getIdUser() {
        return user;
    }

    public void setIdUser(UserDAO id_user) {
        this.user = id_user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserDAO getUser() {
        return user;
    }

    public void setUser(UserDAO user) {
        this.user = user;
    }
}
