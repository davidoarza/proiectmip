package com.university.project.persistance.TODOList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ActivityJPA extends JpaRepository<ActivityDAO, Long> {
    @Query(value = "select a from Activity a")
    List<ActivityDAO> getAllActivities();
}