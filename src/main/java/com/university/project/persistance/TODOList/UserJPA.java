package com.university.project.persistance.TODOList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Interface that defines the repository for client table
 */
public interface UserJPA extends JpaRepository<UserDAO, Long> {
    @Query(value = "select u from User u")
    List<UserDAO> getAllUsers();

}