package com.university.project.persistance.TODOList;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "User")
public class UserDAO implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @OneToMany(mappedBy = "user")
    private List<ListDAO> list = new ArrayList<>();

    public UserDAO() {
    }

    public UserDAO(Long id, String name, List<ListDAO> list) {
        this.id = id;
        this.name = name;
        this.list = list;
    }

    public Long getIdUser() {
        return id;
    }

    public void setIdUser(Long id_user) {
        this.id = id_user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<ListDAO> getList() {
        return list;
    }

    public void setList(List<ListDAO> list) {
        this.list = list;
    }
}
