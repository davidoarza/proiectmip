package com.university.project.persistance.TODOList;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "Activity")
public class ActivityDAO implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @ManyToOne
    ListDAO list ;

    public ActivityDAO() {
    }

    public ActivityDAO(Long id, String name, ListDAO list) {
        this.id = id;
        this.name = name;
        this.list = list;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ListDAO getList() {
        return list;
    }

    public void setList(ListDAO list) {
        this.list = list;
    }
}
