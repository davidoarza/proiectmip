package com.university.project.persistance.TODOList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Interface that defines the repository for client table
 */
public interface ListJPA extends JpaRepository<ListDAO, Long> {
    @Query(value = "select l from List l")
    List<ListDAO> getAllLists();
}