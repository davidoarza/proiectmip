package com.university.project.persistance;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Class used for configuring the persistance package in spring container
 */
@EnableJpaRepositories
@ComponentScan
@EntityScan
@Configuration
public class PersistanceModule {
}
