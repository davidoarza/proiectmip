package com.university.project.web.api;

import com.university.project.model.ListDTO;
import com.university.project.service.ServiceModule;
import com.university.project.service.TODOList.ListService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/list/")
@Import(ServiceModule.class)
@Controller
public class ListAPI {

    private final ListService listService;

    @Autowired
    public ListAPI(ListService listService) {
        this.listService = listService;
    }

    @GetMapping
    List<ListDTO> getLists(){
        return listService.getLists();
    }

    @GetMapping("{id}/")
    ListDTO getList(@PathVariable("id") Long id){
        return listService.getList(id);
    }

    @PostMapping
    ListDTO addList(@RequestBody ListDTO listDTO){
        return listService.addList(listDTO);
    }

    @PutMapping("{id}/")
    ListDTO updateList(@PathVariable("id") Long id, @RequestBody ListDTO listDTO) throws NotFoundException {
        return listService.updateList(id, listDTO);
    }

    @DeleteMapping("{id}")
    void deleteList(@PathVariable(value = "id") Long id) throws NotFoundException {
        listService.delete(id);
    }
}
