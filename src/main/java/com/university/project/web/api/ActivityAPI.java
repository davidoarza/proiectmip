package com.university.project.web.api;

import com.university.project.model.ActivityDTO;
import com.university.project.service.ServiceModule;
import com.university.project.service.TODOList.ActivityService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/activity/")
@Import(ServiceModule.class)
@Controller
public class ActivityAPI {

    private final ActivityService activityService;

    @Autowired
    public ActivityAPI(ActivityService activityService) {
        this.activityService = activityService;
    }

    @GetMapping
    List<ActivityDTO> getActivities(){
        return activityService.getActivities();
    }

    @GetMapping("{id}/")
    ActivityDTO getActivity(@PathVariable("id") Long id){
        return activityService.getActivity(id);
    }

    @PostMapping
    ActivityDTO addActivity(@RequestBody ActivityDTO activityDTO){
        return activityService.addActivity(activityDTO);
    }

    @PutMapping("{id}/")
    ActivityDTO updateActivity(@PathVariable("id") Long id, @RequestBody ActivityDTO activityDTO) throws NotFoundException {
        return activityService.updateActivity(id, activityDTO);
    }

    @DeleteMapping("{id}")
    void deleteActivity(@PathVariable(value = "id") Long id) throws NotFoundException {
        activityService.delete(id);
    }
}
