package com.university.project.web.api;

import com.university.project.model.UserDTO;
import com.university.project.service.ServiceModule;
import com.university.project.service.TODOList.UserService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/user/")
@Import(ServiceModule.class)
@Controller
public class UserAPI {

    private final UserService userService;

    @Autowired
    public UserAPI(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    List<UserDTO> getUsers(){
        return userService.getUsers();
    }

    @GetMapping("{id}/")
    UserDTO getUser(@PathVariable("id") Long id){
        return userService.getUser(id);
    }

    @PostMapping
    UserDTO addUser(@RequestBody UserDTO userDTO){
        return userService.addUser(userDTO);
    }

    @PutMapping("{id}/")
    UserDTO updateUser(@PathVariable("id") Long id, @RequestBody UserDTO userDTO) throws NotFoundException {
        return userService.updateUser(id, userDTO);
    }

    @DeleteMapping("{id}")
    void deleteUser(@PathVariable(value = "id") Long id) throws NotFoundException {
        userService.delete(id);
    }
}
