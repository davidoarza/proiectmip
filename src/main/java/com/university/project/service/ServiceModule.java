package com.university.project.service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.persistence.Persistence;

/**
 * Class used for configuring the service package in spring container
 */
@Configuration
@ComponentScan
@Import(Persistence.class)
public class ServiceModule {
}
