package com.university.project.service.TODOList;

import com.university.project.model.ActivityDTO;
import com.university.project.model.ListDTO;
import com.university.project.persistance.TODOList.*;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ActivityService {
    private final ActivityJPA activityJPA;

    @Autowired
    public ActivityService(ActivityJPA activityJPA) {
        this.activityJPA = activityJPA;
    }

    public List<ActivityDTO> getActivities() {
        return activityJPA
                .getAllActivities()
                .stream()
                .map(activity -> convertToDTO(activity))
                .collect(Collectors.toList());
    }

    public ActivityDTO getActivity(Long id) {
        ActivityDAO activity = activityJPA.getOne(id);
        return convertToDTO(activity);
    }

    public ActivityDTO addActivity(ActivityDTO ActivityDTO) {
        ActivityDAO activity = new ActivityDAO();
        activity = convertToDAO(ActivityDTO);
        ActivityDAO savedActivity = activityJPA.save(activity);
        return convertToDTO(savedActivity);
    }

    public ActivityDTO updateActivity(Long id, ActivityDTO ActivityDTO) throws NotFoundException {
        ActivityDAO activity = activityJPA.getOne(id);
        if (activity == null) {
            throw new NotFoundException("Activity not Found");
        }
        ActivityDTO activity1 = convertToDTO(activity);
        ActivityDAO savedActivity = activityJPA.save(activity);
        return convertToDTO(savedActivity);
    }

    public void delete(Long id) throws NotFoundException {
        ActivityDAO activity = activityJPA.getOne(id);
        if (activity == null) {
            throw new NotFoundException("Activity not Found");
        }
        activityJPA.delete(activity);
    }

    private ActivityDAO convertToDAO(ActivityDTO activityDTO) {
        ActivityDAO activityDAO = new ActivityDAO();
        activityDAO.setName(activityDTO.getActivity());
        return activityDAO;
    }

    private ActivityDTO convertToDTO(ActivityDAO activityDAO) {
        ActivityDTO activityDTO = new ActivityDTO();
        activityDTO.setActivity(activityDAO.getName());
        activityDTO.setId(activityDAO.getId());
        if(activityDAO.getList()!=null) {
            activityDTO.setList(new ListDTO(activityDAO.getList().getId(), activityDTO.getList().getUser(), new ArrayList<ActivityDTO>()));
        }
        return activityDTO;
    }
}
