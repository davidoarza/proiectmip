package com.university.project.service.TODOList;

import com.university.project.model.ListDTO;
import com.university.project.model.UserDTO;
import com.university.project.persistance.TODOList.ActivityDAO;
import com.university.project.persistance.TODOList.ListDAO;
import com.university.project.persistance.TODOList.ListJPA;
import com.university.project.persistance.TODOList.UserDAO;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ListService {
    private final ListJPA listJPA;

    @Autowired
    public ListService(ListJPA listJPA) {
        this.listJPA = listJPA;
    }

    public List<ListDTO> getLists() {
        return listJPA
                .getAllLists()
                .stream()
                .map(list -> convertToDTO(list))
                .collect(Collectors.toList());
    }

    public ListDTO getList(Long id) {
        ListDAO list = listJPA.getOne(id);
        return convertToDTO(list);
    }

    public ListDTO addList(ListDTO ListDTO) {
        ListDAO list = new ListDAO();
        ListDTO list1 = convertToDTO(list);
        ListDAO savedList = listJPA.save(list);
        return convertToDTO(savedList);
    }

    public ListDTO updateList(Long id, ListDTO listDTO) throws NotFoundException {
        ListDAO list = listJPA.getOne(id);
        if (list == null) {
            throw new NotFoundException("List not Found");
        }
        ListDTO list1 = convertToDTO(list);
        ListDAO savedList = listJPA.save(list);
        return convertToDTO(savedList);
    }

    public void delete(Long id) throws NotFoundException {
        ListDAO list = listJPA.getOne(id);
        if (list == null) {
            throw new NotFoundException("List not Found");
        }
        listJPA.delete(list);
    }

    private ListDAO convertToDAO(ListDTO listDTO) {
        ListDAO listDAO = new ListDAO();
        listDAO.setIdList(listDTO.getIdList());
        return listDAO;
    }

    private ListDTO convertToDTO(ListDAO listDAO) {
        ListDTO listDTO = new ListDTO();
        if(listDAO.getId()!=null) {
            listDTO.setId(listDAO.getId());
        }
        return listDTO;
    }
}
