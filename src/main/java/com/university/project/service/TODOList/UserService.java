package com.university.project.service.TODOList;

import com.university.project.model.UserDTO;
import com.university.project.persistance.TODOList.UserDAO;
import com.university.project.persistance.TODOList.UserJPA;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final UserJPA userJPA;

    @Autowired
    public UserService(UserJPA userJPA) {
        this.userJPA = userJPA;
    }

    public List<UserDTO> getUsers() {
        return userJPA
                .getAllUsers()
                .stream()
                .map(user -> convertToDTO(user))
                .collect(Collectors.toList());
    }

    public UserDTO getUser(Long id){
        UserDAO user = userJPA.getOne(id);
        return convertToDTO(user);
    }

    public UserDTO addUser(UserDTO userDTO){
        UserDAO user = new UserDAO();
        user = convertToDAO(userDTO);
        UserDAO savedUser = userJPA.save(user);
        return convertToDTO(savedUser);
    }

    public UserDTO updateUser(Long id, UserDTO userDTO) throws NotFoundException{
        UserDAO user = userJPA.getOne(id);
        if (user == null) {
            throw new NotFoundException("User not Found");
        }
        UserDTO user1 = convertToDTO(user);
        UserDAO savedUser = userJPA.save(user);
        return convertToDTO(savedUser);
    }

    public void delete(Long id) throws NotFoundException{
        UserDAO user = userJPA.getOne(id);
        if (user == null) {
            throw new NotFoundException("User not Found");
        }
        userJPA.delete(user);
    }

    private UserDAO convertToDAO(UserDTO userDTO) {
        UserDAO userDAO = new UserDAO();
        userDAO.setName(userDTO.getName());
        userDAO.setIdUser(userDTO.getIdUser());
        return userDAO;
    }

    private UserDTO convertToDTO(UserDAO userDAO) {
        UserDTO userDTO = new UserDTO();
        userDTO.setName(userDAO.getName());
        if(userDAO.getId()!=null) {
            userDTO.setIdUser(userDAO.getId());
        }
        return userDTO;
    }
}
